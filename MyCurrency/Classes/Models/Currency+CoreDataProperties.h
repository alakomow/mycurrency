//
//  Currency+CoreDataProperties.h
//  MyCurrency
//
//  Created by Artem on 19/10/15.
//  Copyright © 2015 Artem Lakomow. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Currency.h"

NS_ASSUME_NONNULL_BEGIN

@interface Currency (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *code;
@property (nullable, nonatomic, retain) NSString *descriptionTitle;
@property (nullable, nonatomic, retain) NSSet<Converting *> *bases;
@property (nullable, nonatomic, retain) NSSet<Converting *> *references;

@end

@interface Currency (CoreDataGeneratedAccessors)

- (void)addBasesObject:(Converting *)value;
- (void)removeBasesObject:(Converting *)value;
- (void)addBases:(NSSet<Converting *> *)values;
- (void)removeBases:(NSSet<Converting *> *)values;

- (void)addReferencesObject:(Converting *)value;
- (void)removeReferencesObject:(Converting *)value;
- (void)addReferences:(NSSet<Converting *> *)values;
- (void)removeReferences:(NSSet<Converting *> *)values;

@end

NS_ASSUME_NONNULL_END
