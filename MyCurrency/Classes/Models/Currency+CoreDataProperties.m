//
//  Currency+CoreDataProperties.m
//  MyCurrency
//
//  Created by Artem on 19/10/15.
//  Copyright © 2015 Artem Lakomow. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Currency+CoreDataProperties.h"

@implementation Currency (CoreDataProperties)

@dynamic code;
@dynamic descriptionTitle;
@dynamic bases;
@dynamic references;

@end
