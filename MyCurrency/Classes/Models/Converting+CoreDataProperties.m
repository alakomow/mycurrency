//
//  Converting+CoreDataProperties.m
//  MyCurrency
//
//  Created by Artem on 19/10/15.
//  Copyright © 2015 Artem Lakomow. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Converting+CoreDataProperties.h"

@implementation Converting (CoreDataProperties)

@dynamic rate;
@dynamic rateDate;
@dynamic base;
@dynamic reference;

@end
