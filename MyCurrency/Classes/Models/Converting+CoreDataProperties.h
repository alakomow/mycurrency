//
//  Converting+CoreDataProperties.h
//  MyCurrency
//
//  Created by Artem on 19/10/15.
//  Copyright © 2015 Artem Lakomow. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Converting.h"
NS_ASSUME_NONNULL_BEGIN

@interface Converting (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *rate;
@property (nullable, nonatomic, retain) NSDate *rateDate;
@property (nullable, nonatomic, retain) Currency *base;
@property (nullable, nonatomic, retain) Currency *reference;

@end

NS_ASSUME_NONNULL_END
