//
//  ApiMappingOperation.h
//  MyCurrency
//
//  Created by Artem on 19/10/15.
//  Copyright © 2015 Artem Lakomow. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString* const kDefaultMappingErrorDomain;

typedef NS_ENUM(NSUInteger, APIMappingType) {
    APIMappingTypeCurrencyCode = 1,
    APIMappingTypeConvertValue,
    ApiMappingOperationCurrencyDescription
};

typedef void(^APIMappingOperationCompletionBlock)(NSArray *objectsIDs, NSError *error);

@interface ApiMappingOperation : NSOperation
- (id) init __attribute__((unavailable("Must use other init methods.")));
+ (id) init __attribute__((unavailable("Must use other init methods.")));
+ (id) new __attribute__((unavailable("Must use other init methods.")));

- (instancetype)initWithComletion:(APIMappingOperationCompletionBlock)completion;
- (void)mapWithData:(NSDictionary *)data mappingType:(APIMappingType)type;
@end
