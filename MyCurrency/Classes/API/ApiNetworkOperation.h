//
//  ApiOperation.h
//  MyCurrency
//
//  Created by Artem on 19/10/15.
//  Copyright © 2015 Artem Lakomow. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^APINetworkOperationCompletionBlock)(NSDictionary *responce, BOOL didCanceled, NSError *error);

@interface ApiNetworkOperation : NSOperation
- (id) init __attribute__((unavailable("Must use other init methods.")));
+ (id) init __attribute__((unavailable("Must use other init methods.")));
+ (id) new __attribute__((unavailable("Must use other init methods.")));

- (instancetype)initCurrencyListWithCompletion:(APINetworkOperationCompletionBlock)completion;
- (instancetype)initRatesForBaseCurrencyCode:(NSString *)code completion:(APINetworkOperationCompletionBlock)completion;
- (instancetype)initCurrencyDescriptionWithCompletion:(APINetworkOperationCompletionBlock)completion;
@end
