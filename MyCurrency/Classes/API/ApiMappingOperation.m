//
//  ApiMappingOperation.m
//  MyCurrency
//
//  Created by Artem on 19/10/15.
//  Copyright © 2015 Artem Lakomow. All rights reserved.
//

#import "ApiMappingOperation.h"
#import "Currency+CoreDataProperties.h"
#import "Converting+CoreDataProperties.h"
#import <MagicalRecord/MagicalRecord.h>

NSString* const kDefaultMappingErrorDomain = @"MappingerrorDomain";

@interface ApiMappingOperation()
@property (nonatomic, copy) APIMappingOperationCompletionBlock completion;
@property (nonatomic, strong) NSDictionary *mappingData;
@property (nonatomic, assign) APIMappingType mappingType;
@end

@implementation ApiMappingOperation
- (instancetype)initWithComletion:(APIMappingOperationCompletionBlock)completion {
    if (self = [super init]) {
        self.completion = completion;
    }
    return self;
}

- (void)mapWithData:(NSDictionary *)data mappingType:(APIMappingType)type {
    self.mappingData = data;
    self.mappingType = type;
}

- (void)main {

    if ([self.mappingData.allKeys containsObject:@"error"]) {
        NSMutableDictionary* details = [NSMutableDictionary dictionary];
        [details setValue:[self.mappingData objectForKey:@"error"] forKey:NSLocalizedDescriptionKey];
        NSError *error = [NSError errorWithDomain:kDefaultMappingErrorDomain code:-1 userInfo:details];

        __weak typeof(self) weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            if (weakSelf.completion) {
                weakSelf.completion(nil, error);
            }
        });
        return;
    }
    switch (self.mappingType) {
        case APIMappingTypeCurrencyCode:
            [self mapCurrencyList];
            break;
        case APIMappingTypeConvertValue:
            [self mapRates];
            break;

        case ApiMappingOperationCurrencyDescription:
            [self mapCurrencyDescription];
            break;

        default:
            break;
    }

}

#pragma mark - Private
- (void)mapCurrencyList {
    NSMutableArray *mappingData = [NSMutableArray new];
    if ([self.mappingData.allKeys containsObject:@"rates"]) {
        for (NSDictionary *currency in [[self.mappingData objectForKey:@"rates"] allKeys]) {
            [mappingData addObject:@{@"code":currency}];
        }
    }
    if ([self.mappingData.allKeys containsObject:@"base"]) {
        [mappingData addObject:@{@"code":[self.mappingData objectForKey:@"base"]}];
    }

    __block NSArray *objectIDs;
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        objectIDs = [[Currency MR_importFromArray:mappingData inContext:localContext] valueForKeyPath:@"objectID"];
    }];

    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (weakSelf.completion) {
            weakSelf.completion(objectIDs, nil);
        }
    });
}

- (void)mapRates {
    NSString *baseCode = [self.mappingData objectForKey:@"base"];
    NSDictionary *rates = [self.mappingData objectForKey:@"rates"];

    __block NSArray *objectIDs;
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        Currency *baseCurrency = [Currency MR_findFirstByAttribute:@"code" withValue:baseCode inContext:localContext];
        if (baseCurrency && rates) {
            NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"base == %@ AND reference.code in(%@)",baseCurrency,[rates allKeys]];
            NSArray *objects = [Converting MR_findAllWithPredicate:searchPredicate inContext:localContext];
            for (Converting *object in objects) {
                object.rate = [rates objectForKey:object.reference.code];
                object.rateDate = [NSDate new];
            }

            objectIDs = [objects valueForKeyPath:@"objectID"];
        }
    }];

    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (weakSelf.completion) {
            weakSelf.completion(objectIDs, nil);
        }
    });
}

- (void)mapCurrencyDescription {
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        NSArray *objects = [Currency MR_findAllInContext:localContext];
        for (Currency *object in objects) {
            NSString *description = [self.mappingData objectForKey:object.code];
            if (description.length) {
                object.descriptionTitle = description;
            }
            NSLog(@"");
        }
    }];
}
@end
