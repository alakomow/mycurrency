//
//  ApiOperation.m
//  MyCurrency
//
//  Created by Artem on 19/10/15.
//  Copyright © 2015 Artem Lakomow. All rights reserved.
//

#import "ApiNetworkOperation.h"

NSString* const kDefaultApiOperationURL = @"https://api.fixer.io/";
NSString* const kDefaultApiDescriptionOperationURL = @"https://openexchangerates.org/";

@interface ApiNetworkOperation()
@property (nonatomic, strong) NSURLSessionDataTask *dataTask;
@property (nonatomic, strong, readonly) NSURL *operationURL;
@property (nonatomic, copy) APINetworkOperationCompletionBlock completion;
@end

@implementation ApiNetworkOperation

- (instancetype)initWithCompletion:(APINetworkOperationCompletionBlock)completion {
    if (self = [super init]) {
        self.completion = completion;
    }
    return self;
}

- (instancetype)initCurrencyListWithCompletion:(APINetworkOperationCompletionBlock)completion {
    if (self = [self initWithCompletion:completion]) {
        _operationURL = [NSURL URLWithString:[kDefaultApiOperationURL stringByAppendingFormat:@"latest"]];
    }
    return self;
}

- (instancetype)initRatesForBaseCurrencyCode:(NSString *)code completion:(APINetworkOperationCompletionBlock)completion {
    if (self = [self initWithCompletion:completion]) {
        _operationURL = [NSURL URLWithString:[kDefaultApiOperationURL stringByAppendingFormat:@"latest?base=%@",code]];
    }
    return self;
}

- (instancetype)initCurrencyDescriptionWithCompletion:(APINetworkOperationCompletionBlock)completion {
    if (self = [self initWithCompletion:completion]) {
        _operationURL = [NSURL URLWithString:[kDefaultApiDescriptionOperationURL stringByAppendingFormat:@"currencies.json"]];
    }
    return self;
}

- (void)main {
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);

    NSURLSession *session = [NSURLSession sharedSession];
    __weak typeof(self) weakSelf = self;
    self.dataTask = [session dataTaskWithURL:self.operationURL completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){


        if (error) {
            BOOL isCanceled = NO;
            if ([error.domain isEqualToString:NSURLErrorDomain] && error.code == NSURLErrorCancelled) {
                isCanceled = YES;
            }

            dispatch_async(dispatch_get_main_queue(), ^{
                if (weakSelf.completion) {
                    weakSelf.completion(nil,isCanceled,error);
                }
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    dispatch_semaphore_signal(semaphore);
                });
            });
            return;
        }


        NSDictionary *responceData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (weakSelf.completion) {
                weakSelf.completion(responceData, NO, error);
            }
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                dispatch_semaphore_signal(semaphore);
            });
        });
    }];
    [self.dataTask resume];
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
}

- (void)cancel {
    [self.dataTask cancel];
    [super cancel];
}
@end
