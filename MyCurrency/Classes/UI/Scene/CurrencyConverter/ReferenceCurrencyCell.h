//
//  ReferenceCurrencyCell.h
//  MyCurrency
//
//  Created by Artem on 19/10/15.
//  Copyright © 2015 Artem Lakomow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReferenceCurrencyCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *currencyCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *currencyDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *currencyConvertValue;
@property (weak, nonatomic) IBOutlet UILabel *currencyRateDate;

@end
