//
//  CurrencyConverterViewController.m
//  MyCurrency
//
//  Created by Artem on 19/10/15.
//  Copyright © 2015 Artem Lakomow. All rights reserved.
//

#import "CurrencyConverterViewController.h"
#import "NSUserDefaults+CurrencyData.h"

#import "Currency+CoreDataProperties.h"
#import "Converting+CoreDataProperties.h"

#import "ApiNetworkOperation.h"
#import "ApiMappingOperation.h"

#import "CurrencyListViewController.h"
#import "ReferenceCurrencyCell.h"

#import "UIAlertController+ErrorMessage.h"

#import <MagicalRecord/MagicalRecord.h>

#define kListCurrencyControllerIdentifier @"OpenCurrencyList"

typedef NS_ENUM(NSUInteger, SelectCurrencyType) {
    SelectCurrencyTypeChangeBase,
    SelectCurrencyTypeAddNewReference
};

@interface CurrencyConverterViewController() <CurrencyListViewControllerDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate, UITextFieldDelegate, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *currencyCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *currencyDescriptionLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *currencyValueField;

@property (nonatomic, strong) Currency *baseCurrency;
@property (nonatomic, strong) NSDictionary *ratesData;

@property (nonatomic, strong, readonly) NSOperationQueue *controllerQueue;
@property (nonatomic, assign) SelectCurrencyType selectCurrencyType;

@property (nonatomic, strong) NSFetchedResultsController *resultController;

@property (nonatomic, strong) NSNumberFormatter *numberFormater;
@property (nonatomic, strong) NSDateFormatter *dateFormater;

@end

@implementation CurrencyConverterViewController


- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        _controllerQueue = [NSOperationQueue new];
        self.controllerQueue.name = [NSStringFromClass([self class]) stringByAppendingString:@".controllerQueue"];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.tableView.estimatedRowHeight = 81.0f;
    self.tableView.rowHeight = UITableViewAutomaticDimension;

    self.numberFormater = [NSNumberFormatter new];
    [self.numberFormater setMaximumFractionDigits:3];
    [self.numberFormater setMaximumIntegerDigits:6];
    [self.numberFormater setRoundingMode: NSNumberFormatterRoundUp];

    self.dateFormater = [NSDateFormatter new];
    self.dateFormater.dateStyle = NSDateFormatterShortStyle;
    self.dateFormater.timeStyle = NSDateFormatterNoStyle;

    [self updateBaseCurrencyWithCode:[NSUserDefaults standardUserDefaults].baseCurrencyCode];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeBaseCurrency:) name:kNSUserDefaultsDidChangeBaseCurrencyCode object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Actions
- (IBAction)changeBaseCurrency:(id)sender {
    self.selectCurrencyType = SelectCurrencyTypeChangeBase;
    [self performSegueWithIdentifier:kListCurrencyControllerIdentifier sender:nil];
}
- (IBAction)addReferenceCurrency:(id)sender {
    self.selectCurrencyType = SelectCurrencyTypeAddNewReference;
    [self performSegueWithIdentifier:kListCurrencyControllerIdentifier sender:nil];
}
- (IBAction)currencyValueChanged:(UITextField *)sender {
    [self.tableView reloadRowsAtIndexPaths:self.tableView.indexPathsForVisibleRows withRowAnimation:UITableViewRowAnimationFade];
}
- (IBAction)reloadData:(id)sender {
    [self updateBaseCurrencyWithCode:self.baseCurrency.code];
}



#pragma mark - Private
- (void)updateBaseCurrencyWithCode:(NSString *)code {

    NSString *defaultBaseCodeMessage = @"Set base currency";
    NSString *baseCode = code?:defaultBaseCodeMessage;
    NSString *baseDescription = @"";

    self.baseCurrency = [Currency MR_findFirstByAttribute:@"code" withValue:baseCode inContext:[NSManagedObjectContext MR_defaultContext]];
    if (self.baseCurrency) {
        baseCode = self.baseCurrency.code;
        baseDescription = self.baseCurrency.descriptionTitle;

       self.resultController = [Converting MR_fetchAllGroupedBy:nil withPredicate:[NSPredicate predicateWithFormat:@"base == %@",self.baseCurrency] sortedBy:@"reference.code" ascending:YES delegate:self];
       [self.tableView reloadData];

    }

    self.currencyCodeLabel.text = baseCode;
    self.currencyDescriptionLabel.text = baseDescription;

    self.ratesData = nil;
    if ([baseCode isEqualToString:defaultBaseCodeMessage]) {
        return;
    }
    ApiMappingOperation *mappingOperation = [[ApiMappingOperation alloc] initWithComletion:^(NSArray *objectsIDs, NSError *error) {
        if (error) {
            UIAlertController *alert = [UIAlertController alertWithError:error];
            [self presentViewController:alert animated:YES completion:NULL];
        }
    }];

    __weak typeof(self) weakSelf = self;
    ApiNetworkOperation *apiOperation = [[ApiNetworkOperation alloc]initRatesForBaseCurrencyCode:baseCode completion:^(NSDictionary *responce, BOOL didCanceled, NSError *error) {
        if (didCanceled) {
            return;
        }
        if (error) {
            UIAlertController *alert = [UIAlertController alertWithError:error];
            [self presentViewController:alert animated:YES completion:NULL];
            return;
        }
        [mappingOperation mapWithData:responce mappingType:APIMappingTypeConvertValue];
        weakSelf.ratesData = responce;

    }];
    [mappingOperation addDependency:apiOperation];

    [self.controllerQueue addOperation:apiOperation];
    [self.controllerQueue addOperation:mappingOperation];
}

- (void)saveBaseCurrencyCode:(NSString *)code {
    [NSUserDefaults standardUserDefaults].baseCurrencyCode = code;
}

- (void)addNewReferenceCurrencyCode:(NSString *)code {
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        Currency *localeNewCurrency = [Currency MR_findFirstByAttribute:@"code" withValue:code inContext:localContext];
        Currency *localeBaseCurrency = [self.baseCurrency MR_inContext:localContext];
        if (localeBaseCurrency && localeNewCurrency) {
            Converting *newConverting = [Converting MR_createEntityInContext:localContext];
            newConverting.base = localeBaseCurrency;
            newConverting.reference = localeNewCurrency;
            newConverting.rate = @(0.0);
            newConverting.rateDate = [NSDate dateWithTimeIntervalSince1970:1.f];
        }
    }];

    ApiMappingOperation *mappingOperation = [[ApiMappingOperation alloc] initWithComletion:^(NSArray *objectsIDs, NSError *error) {
        if (error) {
            UIAlertController *alert = [UIAlertController alertWithError:error];
            [self presentViewController:alert animated:YES completion:NULL];
        }
    }];
    [mappingOperation mapWithData:self.ratesData mappingType:APIMappingTypeConvertValue];
    [self.controllerQueue addOperation:mappingOperation];
}

- (void)configureCell:(ReferenceCurrencyCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Converting *converting = [self.resultController objectAtIndexPath:indexPath];

    cell.currencyCodeLabel.text = converting.reference.code;
    cell.currencyDescriptionLabel.text = converting.reference.descriptionTitle;
    NSNumber *convertingSumm = self.currencyValueField.text.length?@([self.currencyValueField.text doubleValue]):@([self.currencyValueField.placeholder doubleValue]);
    NSNumber *convertedSumm = @(convertingSumm.doubleValue * converting.rate.doubleValue);

    cell.currencyConvertValue.text = [NSString stringWithFormat:@"%@ %@ → %@ %@",
                                      [convertingSumm stringValue],
                                      converting.base.code,
                                      [self.numberFormater stringFromNumber:convertedSumm],
                                      converting.reference.code];

    cell.currencyRateDate.text = [self.dateFormater stringFromDate:converting.rateDate];

}

#pragma mark - Observers
- (void)didChangeBaseCurrency:(NSNotification *)notification {
    NSString *newBaseCode = notification.object;
    [self updateBaseCurrencyWithCode:newBaseCode];
}

#pragma mark - CurrencyListViewControllerDelegate
- (void)currencyListViewController:(CurrencyListViewController *)controller didSelectCurrencyCode:(NSString *)code {

    switch (self.selectCurrencyType) {
        case SelectCurrencyTypeChangeBase:
             [self saveBaseCurrencyCode:code];
            break;

        case SelectCurrencyTypeAddNewReference:
            [self addNewReferenceCurrencyCode:code];
            break;

        default:
            break;
    }
    [controller.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id  sectionInfo =
    [[self.resultController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"ReferenceCurrencyCell";
    ReferenceCurrencyCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Converting *converting = [self.resultController objectAtIndexPath:indexPath];
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            Converting *localConverting = [converting MR_inContext:localContext];
            [localConverting MR_deleteEntityInContext:localContext];
        }];
    }
}

#pragma mark - NSFetchedResultsControllerDelegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {

    UITableView *tableView = self.tableView;

    switch(type) {

        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;

        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray
                                               arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray
                                               arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [self.tableView endUpdates];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *value = [textField.text stringByAppendingString:string];

    NSArray *decimapParts = [value componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"."]];
    if (decimapParts.count > 2) {
        return NO;
    } else if (decimapParts.count == 2) {
        if ([[decimapParts objectAtIndex:0] length] > 7) {
            return 0;
        }

        if ([[decimapParts objectAtIndex:1] length] > 2) {
            return NO;
        }
    }else if (decimapParts.count == 1){
        if ([[decimapParts objectAtIndex:0] length] > 7) {
            return 0;
        }
    }
    return YES;
}

 #pragma mark - Navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     if ([segue.identifier isEqualToString:kListCurrencyControllerIdentifier]) {
         CurrencyListViewController *controller = segue.destinationViewController;
         switch (self.selectCurrencyType) {
             case SelectCurrencyTypeChangeBase:
                 controller.navigationItem.title = @"Change base currency";
                 break;
            case SelectCurrencyTypeAddNewReference:
                 controller.navigationItem.title = @"Add new reference currency";
                 controller.escapedCodes = [[[[self.resultController fetchedObjects]
                                              valueForKeyPath:@"reference"]
                                             valueForKeyPath:@"code"]
                                            arrayByAddingObject:self.baseCurrency.code];
                 break;
                 
             default:
                 break;
         }
         controller.delegate = self;
     }
 }
@end
