//
//  CurrencyListViewController.m
//  MyCurrency
//
//  Created by Artem on 18/10/15.
//  Copyright © 2015 Artem Lakomow. All rights reserved.
//

#import "CurrencyListViewController.h"
#import "ApiNetworkOperation.h"
#import "ApiMappingOperation.h"
#import "Currency+CoreDataProperties.h"
#import "UIAlertController+ErrorMessage.h"


#import <SVProgressHUD/SVProgressHUD.h>
#import <MagicalRecord/MagicalRecord.h>


@interface CurrencyListViewController () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UISearchResultsUpdating>
@property (nonatomic, strong, readonly) NSOperationQueue *controllerQueue;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSFetchedResultsController *resultController;

@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) NSArray *searchedContent;
@end

@implementation CurrencyListViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        _controllerQueue = [NSOperationQueue new];
        self.controllerQueue.name = [NSStringFromClass([self class]) stringByAppendingString:@".controllerQueue"];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    [self.searchController.searchBar sizeToFit];


    self.tableView.tableHeaderView = self.searchController.searchBar;
    [self.tableView reloadData];
    if (!self.escapedCodes.count) {
        self.escapedCodes = nil;
    }

    if ([self tableView:self.tableView numberOfRowsInSection:0]) {
        return;
    }

    ApiMappingOperation *apiMappingCurrencysOperation = [[ApiMappingOperation alloc] initWithComletion:^(NSArray *objectsIDs, NSError *error) {
        if (error) {
            UIAlertController *alert = [UIAlertController alertWithError:error];
            [self presentViewController:alert animated:YES completion:NULL];
        }
    }];
    ApiNetworkOperation *apiOperation = [[ApiNetworkOperation alloc] initCurrencyListWithCompletion:^(NSDictionary *responce, BOOL didCanceled, NSError *error) {
        if (didCanceled) {
            return;
        }
        if (error) {
            UIAlertController *alert = [UIAlertController alertWithError:error];
            [self presentViewController:alert animated:YES completion:NULL];
            return;
        }
        [apiMappingCurrencysOperation mapWithData:responce mappingType:APIMappingTypeCurrencyCode];
    }];

    ApiMappingOperation *apiDescriptionCurrencyMapping = [[ApiMappingOperation alloc] initWithComletion:^(NSArray *objectsIDs, NSError *error) {
        if (error) {
            UIAlertController *alert = [UIAlertController alertWithError:error];
            [self presentViewController:alert animated:YES completion:NULL];
        }
    }];
    ApiNetworkOperation *currencyDescriptionOperation = [[ApiNetworkOperation alloc] initCurrencyDescriptionWithCompletion:^(NSDictionary *responce, BOOL didCanceled, NSError *error) {
        if (didCanceled) {
            return;
        }
        if (error) {
            UIAlertController *alert = [UIAlertController alertWithError:error];
            [self presentViewController:alert animated:YES completion:NULL];
            return;
        }
        [apiDescriptionCurrencyMapping mapWithData:responce mappingType:ApiMappingOperationCurrencyDescription];

    }];


    [apiMappingCurrencysOperation addDependency:apiOperation];
    [currencyDescriptionOperation addDependency:apiMappingCurrencysOperation];
    [apiDescriptionCurrencyMapping addDependency:currencyDescriptionOperation];
    NSBlockOperation *showHud = [NSBlockOperation blockOperationWithBlock:^{
        [SVProgressHUD showWithStatus:@"Loading data..."];
    }];

    NSBlockOperation *hideHud = [NSBlockOperation blockOperationWithBlock:^{
        [SVProgressHUD dismiss];
    }];
    [apiOperation addDependency:showHud];
    [hideHud addDependency:apiMappingCurrencysOperation];
    [[NSOperationQueue mainQueue] addOperation:showHud];
    [[NSOperationQueue mainQueue] addOperation:hideHud];

    [self.controllerQueue addOperation:apiOperation];
    [self.controllerQueue addOperation:apiMappingCurrencysOperation];
    [self.controllerQueue addOperation:currencyDescriptionOperation];
    [self.controllerQueue addOperation:apiDescriptionCurrencyMapping];

}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.controllerQueue cancelAllOperations];
    self.searchController.active = NO;
}

#pragma mark - Setters
- (void)setEscapedCodes:(NSArray *)escapedCodes {
    _escapedCodes = escapedCodes;

    NSPredicate *escapePredicate;
    if (_escapedCodes.count) {
        escapePredicate = [NSPredicate predicateWithFormat:@"NOT (code in(%@))",_escapedCodes];
    }

    self.resultController = [Currency MR_fetchAllSortedBy:@"code" ascending:YES withPredicate:escapePredicate groupBy:nil delegate:self];


}
#pragma mark - Private
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Currency *currency;
    if (self.searchController.isActive) {
        currency = [self.searchedContent objectAtIndex:indexPath.row];
    } else {
        currency = [self.resultController objectAtIndexPath:indexPath];
    }
    cell.detailTextLabel.text = currency.descriptionTitle;
    cell.textLabel.text = currency.code;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if (self.searchController.isActive) {
        return self.searchedContent.count;
    }
    id  sectionInfo =
    [[self.resultController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"CurrencyCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Currency *currency;
    if (self.searchController.isActive) {
        currency = [self.searchedContent objectAtIndex:indexPath.row];
    } else {
        currency= [self.resultController objectAtIndexPath:indexPath];
    }
    if ([self.delegate respondsToSelector:@selector(currencyListViewController:didSelectCurrencyCode:)]) {
        [self.delegate currencyListViewController:self didSelectCurrencyCode:currency.code];
    }
}

#pragma mark - NSFetchedResultsControllerDelegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {

    UITableView *tableView = self.tableView;

    switch(type) {

        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;

        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray
                                               arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray
                                               arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [self.tableView endUpdates];
}

#pragma mark - UISearchResultsUpdating
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    self.searchedContent = nil;
    if (searchController.searchBar.text.length) {
        NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"(code CONTAINS[c] %@) OR (descriptionTitle CONTAINS[c] %@)", searchController.searchBar.text, searchController.searchBar.text];

        self.searchedContent = [[self.resultController fetchedObjects] filteredArrayUsingPredicate:searchPredicate];
    }else {
        self.searchedContent = [self.resultController fetchedObjects];
    }
    [self.tableView reloadData];
}

@end
