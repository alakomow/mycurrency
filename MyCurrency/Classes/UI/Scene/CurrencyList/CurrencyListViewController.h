//
//  CurrencyListViewController.h
//  MyCurrency
//
//  Created by Artem on 18/10/15.
//  Copyright © 2015 Artem Lakomow. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CurrencyListViewControllerDelegate;
@class Currency;

@interface CurrencyListViewController : UIViewController
@property(nonatomic, weak) id<CurrencyListViewControllerDelegate> delegate;
@property(nonatomic, strong) NSArray *escapedCodes;
@end

@protocol CurrencyListViewControllerDelegate <NSObject>
- (void)currencyListViewController:(CurrencyListViewController *)controller didSelectCurrencyCode:(NSString *)code;
@end
