//
//  UIAlertController+ErrorMessage.h
//  MyCurrency
//
//  Created by Artem on 19/10/15.
//  Copyright © 2015 Artem Lakomow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertController (ErrorMessage)
+ (instancetype)alertWithError:(NSError *)error;
@end
