//
//  UIAlertController+ErrorMessage.m
//  MyCurrency
//
//  Created by Artem on 19/10/15.
//  Copyright © 2015 Artem Lakomow. All rights reserved.
//

#import "UIAlertController+ErrorMessage.h"

@implementation UIAlertController (ErrorMessage)
+(instancetype)alertWithError:(NSError *)error {
    UIAlertController *alert = [self alertControllerWithTitle:@"Error"
                                                                   message:error.localizedDescription
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alert dismissViewControllerAnimated:YES completion:NULL];
    }];

    [alert addAction:okAction];
    return alert;
}
@end
