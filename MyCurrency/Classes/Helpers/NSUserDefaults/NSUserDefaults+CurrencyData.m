//
//  NSUserDefaults+CurrencyData.m
//  MyCurrency
//
//  Created by Artem on 19/10/15.
//  Copyright © 2015 Artem Lakomow. All rights reserved.
//
#import "NSUserDefaults+CurrencyData.h"

NSString* const kNSUserDefaultsDidChangeBaseCurrencyCode = @"kNSUserDefaultsDidChangeBaseCurrencyCode";
#define kKeyForCurrentCurrency @"kKeyForCurrentCurrency"

@implementation NSUserDefaults (CurrencyData)
- (NSString*)baseCurrencyCode {
    return [self objectForKey:kKeyForCurrentCurrency];
}

- (void)setBaseCurrencyCode:(NSString *)baseCurrencyCode {
    [self setObject:baseCurrencyCode forKey:kKeyForCurrentCurrency];
    [self synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNSUserDefaultsDidChangeBaseCurrencyCode object:baseCurrencyCode];
}
@end
