//
//  NSUserDefaults+CurrencyData.h
//  MyCurrency
//
//  Created by Artem on 19/10/15.
//  Copyright © 2015 Artem Lakomow. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString* const kNSUserDefaultsDidChangeBaseCurrencyCode;

@interface NSUserDefaults (CurrencyData)
@property (nonatomic, strong)NSString *baseCurrencyCode;
@end
