//
//  AppDelegate.h
//  MyCurrency
//
//  Created by Artem on 14/10/15.
//  Copyright © 2015 Artem Lakomow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

